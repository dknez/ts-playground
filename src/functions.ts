console.log("let's go!");

class Department {
    name: string;

    constructor(n: string) {
        this.name = n;
    }
}

const accounting = new Department("Accounting");

console.log(accounting);



function add(n1: number, n2: number) {
    return n1 + n2;
}

function printResult(num: number) {
    console.log("Result: " + num);
}

printResult(add(5, 12))


let combineValues: Function;
combineValues = add;
console.log(combineValues(8, 8));

let combineValues1: (a: number, b: number) => number;

function addAndHandle(n1: number, n2: number, callbackFunction: (num: number) => void) {
    const result = n1 + n2;
    callbackFunction(result);
}

addAndHandle(10, 20, (result) => { console.log("result is: " + result); })