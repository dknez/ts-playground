function merge<T, U>(objA: T, objB: U) {
    return Object.assign(objA, objB)
}

console.log(merge({ name: "Max" }, { age: 20 }));

const mergedObje = merge({ name: "Max" }, { age: 30 })